import { Component } from '@angular/core';
import { CueCardComponent } from '../components/cue-card/cue-card.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public segmentView: boolean = false;

  constructor() { }

  toggleSegmentView() {
    this.segmentView = !this.segmentView;
  }

}
