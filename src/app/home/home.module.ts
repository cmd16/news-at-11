import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { PlayerComponent } from '../components/player/player.component';
import { CueCardComponent } from '../components/cue-card/cue-card.component';
import { SegmentComponent } from '../components/segment/segment.component';
import { SegmentViewerComponent } from '../components/segment-viewer/segment-viewer.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, PlayerComponent, CueCardComponent, SegmentComponent, SegmentViewerComponent]
})
export class HomePageModule { }
