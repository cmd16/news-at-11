import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

export interface Card {
  blank1: string,
  blank1d: string,
  blank2: string,
  blank2d: string,
  name: string,
  game?: string,
  used?: boolean,
  presented?: boolean
}

@Injectable({
  providedIn: 'root'
})
export class DeckService {

  public gameCode: string;
  private cardCollection: AngularFirestoreCollection;
  private cards: Card[];
  private availableCards: Card[];
  private availableGameCards: Card[];
  private unavailableGameCards: Card[];
  public presented_cards_subj: BehaviorSubject<Card[]>;
  public dataReadySubj: BehaviorSubject<boolean>;
  public segmentSubj: BehaviorSubject<string[]>;

  constructor(private db: AngularFirestore) {
    this.gameCode = "test0";
    this.dataReadySubj = new BehaviorSubject<boolean>(false);
    this.cardCollection = this.db.collection<Card>("cards");
    this.presented_cards_subj = new BehaviorSubject<Card[]>([]);
    this.segmentSubj = new BehaviorSubject([]);
    this.cardCollection.valueChanges().subscribe(result => {
      this.cards = [];
      this.availableCards = [];
      this.availableGameCards = [];
      this.unavailableGameCards = [];
      const presented_cards = [];
      result.forEach((item: Card) => {
        this.cards.push(item);
        if (!item.used) {
          this.availableCards.push(item);
        }
        if (item.game === this.gameCode) {
          if (!item.used) {
            this.availableGameCards.push(item);
          } else {
            this.unavailableGameCards.push(item);
          }
          if (item.presented) {
            presented_cards.push(item);
          }
        }
      });
      if (!this.dataReadySubj.value) {
        this.dataReadySubj.next(true);
      }
      this.presented_cards_subj.next(presented_cards);
      console.log("presented cards", presented_cards);
    });
    this.db.collection("segments").valueChanges().subscribe(data => {
      const segments = [];
      data.forEach((item: { "name": string }) => {
        segments.push(item.name);
      });
      this.segmentSubj.next(segments);
    });
  }

  public getInitialCard() {
    const card = this.availableCards[Math.floor(Math.random() * this.availableCards.length)];
    this.cardCollection.doc(card.name).update({ used: true });
    return card;
  }

  public submitCard(card: Card) {
    this.cardCollection.doc(card.name).update({ blank1: card.blank1, blank2: card.blank2, game: this.gameCode });
  }

  public getGameCard() {
    const card = this.availableGameCards[Math.floor(Math.random() * this.availableGameCards.length)];
    this.cardCollection.doc(card.name).update({ used: true });
    return card;
  }

  public returnCard(card) {
    this.cardCollection.doc(card.name).update({ used: false });
  }

  public setGameCode(code: string) {
    this.gameCode = code;
  }

  public presentCard(card) {
    this.cardCollection.doc(card.name).update({ presented: true });
    console.log("present card service");
  }

  public hideCard(card) {
    this.cardCollection.doc(card.name).update({ presented: false });
  }
}
