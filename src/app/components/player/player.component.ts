import { Component, OnInit } from '@angular/core';
import { DeckService, Card } from 'src/app/services/deck.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit {
  public cards: Card[] = [];
  public presented_cards: Card[];
  private receivedCard: boolean = false;
  public presenting: boolean = false;
  public submittedCards: boolean = false;

  constructor(public dataService: DeckService) {
    dataService.dataReadySubj.subscribe(value => {
      if (!value || this.receivedCard) {
        return;
      }
      for (let i = 0; i < 3; i++) {
        this.cards.push(dataService.getInitialCard());
        this.receivedCard = true;
      }
    });
    dataService.presented_cards_subj.subscribe(value => {
      this.presented_cards = value;
    });
  }

  ngOnInit() { }

  public async getCues(num: number) {
    console.log("getting cues");
    this.submittedCards = true;
    const timer = ms => new Promise(res => setTimeout(res, ms));
    this.returnCards();
    for (let i = 0; i < num; i++) {
      this.cards.push(this.dataService.getGameCard());
      await timer(500);
    }
  }

  public returnCards() {
    this.cards.forEach(card => {
      this.dataService.returnCard(card);
    });
    this.cards = [];
  }

  public setGameCode(event) {
    this.dataService.setGameCode(event.target.value);
  }

  public presentCards() {
    this.cards.forEach(card => {
      this.dataService.presentCard(card);
    });
    this.presenting = true;
    console.log("presenting");
  }

  public hideCards() {
    this.cards.forEach(card => {
      this.dataService.hideCard(card);
    });
    this.presenting = false;
  }
}
