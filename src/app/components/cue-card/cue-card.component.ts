import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { DeckService } from 'src/app/services/deck.service';

@Component({
  selector: 'app-cue-card',
  templateUrl: './cue-card.component.html',
  styleUrls: ['./cue-card.component.scss'],
})
export class CueCardComponent implements OnInit {

  @Input() blank1: string;
  @Input() blank1d: string;
  @Input() blank2: string;
  @Input() blank2d: string;
  @Input() name: string;
  @Input() dsbl: boolean = false;
  public submitted: boolean = false;

  constructor(public dataService: DeckService) {
  }

  ngOnInit() { }

  public change1(event) {
    this.blank1 = event.target.value;
  }

  public change2(event) {
    this.blank2 = event.target.value;
  }

  public submitCard() {
    console.log(`${this.blank1}\n${this.blank2}`);
    this.dataService.submitCard({
      blank1: this.blank1, blank1d: this.blank1d,
      blank2: this.blank2, blank2d: this.blank2d, name: this.name
    });
    this.submitted = true;
  }

}
