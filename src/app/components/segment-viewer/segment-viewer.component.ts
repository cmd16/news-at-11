import { Component, OnInit } from '@angular/core';
import { DeckService } from 'src/app/services/deck.service';

@Component({
  selector: 'app-segment-viewer',
  templateUrl: './segment-viewer.component.html',
  styleUrls: ['./segment-viewer.component.scss'],
})
export class SegmentViewerComponent implements OnInit {

  private available_segments: string[] = [];
  private used_segments: string[] = [];
  public currentSegment: string;

  constructor(dataService: DeckService) {
    dataService.segmentSubj.subscribe(data => {
      this.available_segments = [];
      this.used_segments = [];
      data.forEach((item: string) => {
        this.available_segments.push(item);
      });
    });
  }

  ngOnInit() { }

  public getNextSegment() {
    const index = Math.floor(Math.random() * this.available_segments.length);
    const segment = this.available_segments.splice(index, 1)[0];
    console.log(segment, index);
    this.currentSegment = segment;
    this.used_segments.push(segment);
    if (this.available_segments.length === 0) {
      this.available_segments = this.used_segments.slice();
      this.used_segments = [];
    }
  }

}
