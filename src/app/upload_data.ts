// @ts-ignore
const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

// Initialize Cloud Firestore through Firebase
// @ts-ignore
firebase.initializeApp({
  apiKey: "AIzaSyBTn5VbPJjwSy5B4ah6jftpVCBnhDG8H0E",
  authDomain: "news-at-11.firebaseapp.com",
  projectId: "news-at-11"
});

// @ts-ignore
const db = firebase.firestore();

const data = require('/Users/cat/newsat11_cards.json');
console.log(data);

data.forEach((obj) => {
  const name = obj.name;
  console.log(name);
  // db.collection("cards").doc(name).update({
  //   used: false,
  //   game: ""
  // }).catch((error) => {
  //   console.error("Error updating document: ", error);
  // });
  db.collection("cards").doc(name).set({
    blank1: obj.blank1,
    blank1d: obj.blank1d,
    blank2: obj.blank2,
    blank2d: obj.blank2d,
    name: obj.name,
    used: false,
    game: "",
    presented: false
  }).catch((error) => {
    console.error("Error adding document: ", error);
  });
});